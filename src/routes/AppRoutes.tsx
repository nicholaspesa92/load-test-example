import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { Create, View } from './../pages';

export const AppRoutes = () => {
    return (
        <div style={{width: '100%', maxWidth: '100%'}}>
            <Route path={'/'} render={() => <Redirect to={'/view-clusters'} />} />
            <Route path={'/create-cluster'} key={'create-cluster'} component={Create} />
            <Route path={'/view-clusters'} key={'view-clusters'} component={View} />
        </div>
    );
};
