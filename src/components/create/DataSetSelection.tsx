import React from 'react';
import './DataSetSelection.scss';
import { ClusterDataset } from '../../models';

interface IDataSetSelection {
    dataset: ClusterDataset;
    onSelect: (dataset: ClusterDataset) => void;
}

export const DataSetSelection = ({
    dataset,
    onSelect,
}: IDataSetSelection) => {

    const onDatasetChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
        e.persist();
        onSelect(e.target.value as ClusterDataset);
    }

    return (
        <div className={'dataset-selection'}>
            <span className={'selection-header'}>Dataset</span>
            <select value={dataset} onChange={onDatasetChange}>
                <option
                    value={ClusterDataset.US_CENSUS}
                >
                        {ClusterDataset.US_CENSUS}
                </option>
                <option
                    value={ClusterDataset.SYNTHENTIC_USER}
                >
                        {ClusterDataset.SYNTHENTIC_USER}
                </option>
            </select>
        </div>
    );
}
