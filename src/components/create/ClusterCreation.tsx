import React, { useState, useEffect } from 'react';
import './ClusterCreation.scss';

import { useAppState, useAppDispatch } from '../../store/AppContext';
import { ClusterTier, ActionType, ClusterDataset } from '../../models';
import { TierSelection } from './TierSelection';
import { DataSetSelection } from './DataSetSelection';
import { ClusterCreateActions } from './ClusterCreateActions';

export const ClusterCreation = () => {

    const state = useAppState();
    const dispatch = useAppDispatch();

    const [selectedTier, setSelectedTier] = useState(state.clusterInfo.tier);
    const [selectedDataset, setSelectedDataset] = useState(state.clusterInfo.dataset);

    useEffect(() => {
        console.log(state);
    }, [state]);

    const onTierSelection = (tier: ClusterTier) => {
        dispatch({type: ActionType.SET_CLUTER_TIER, tier});
        setSelectedTier(tier);
    };

    const onDatasetSelection = (dataset: ClusterDataset) => {
        dispatch({ type: ActionType.SET_CLUSTER_DATASET, dataset});
        setSelectedDataset(dataset);
    };

    return (
        <div className={'create-wrapper'}>
            <div className={'create-section'}>
                <span className={'create-header'}>Create Cluster</span>
                <div className={'create-content'}>
                    <TierSelection tier={selectedTier} onSelect={onTierSelection} />
                    <DataSetSelection dataset={selectedDataset} onSelect={onDatasetSelection} />
                </div>
                <div className={'create-footer'}>
                    <ClusterCreateActions />
                </div>
            </div>
        </div>
    );
};
