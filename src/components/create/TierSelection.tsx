import React from 'react';
import { ClusterTier } from '../../models';
import './TierSelection.scss';

interface ITierSelectionProps {
    tier: ClusterTier
    onSelect: (tier: ClusterTier) => void;
}

export const TierSelection = ({tier, onSelect}: ITierSelectionProps) => {

    return (
        <div className={'tier-selection'}>
            <span className={'selection-header'}>Compute Size</span>
            <span className={'selection-sub-header'}>Choose the compute power of your database. Higher tiers provide higher throughput at lower latency.</span>
            <div className={'option'}>
                <input
                    type={'radio'}
                    checked={tier === ClusterTier.STARTUP}
                    onChange={() => onSelect(ClusterTier.STARTUP)}
                />
                <span className={'label'}>{ClusterTier.STARTUP}</span>
            </div>
            <div className={'option'}>
                <input
                    type={'radio'}
                    checked={tier === ClusterTier.STANDARD}
                    onChange={() => onSelect(ClusterTier.STANDARD)}
                />
                <span className={'label'}>{ClusterTier.STANDARD}</span>
            </div>
            <div className={'option'}>
                <input
                    type={'radio'}
                    checked={tier === ClusterTier.ENTERPRISE}
                    onChange={() => onSelect(ClusterTier.ENTERPRISE)}
                />
                <span className={'label'}>{ClusterTier.ENTERPRISE}</span>
            </div>
        </div>
    );
};
