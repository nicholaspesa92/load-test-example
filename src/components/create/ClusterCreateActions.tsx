import React from 'react';
import './ClusterCreateActions.scss';
import { useAppState, useAppDispatch } from './../../store/AppContext';
import { Cluster, ActionType, ProvisionStep } from '../../models';
import history from './../../history';

export const ClusterCreateActions = () => {

    const state = useAppState();
    const dispatch = useAppDispatch();

    const provision = (cluster: Cluster) => {
        dispatch({ type: ActionType.ADD_CLUSTER, cluster });
        dispatch({ type: ActionType.SET_PROVISIONING, isProvisioning: true });
        history.push('/view-clusters');
        setTimeout(() => {
            dispatch({ type: ActionType.SET_PROVISION_STEP, step: ProvisionStep.TOKEN_RANGES });
            setTimeout(() => {
                dispatch({ type: ActionType.SET_PROVISION_STEP, step: ProvisionStep.COMMUNICATING_TO_CLUSTER });
                setTimeout(() => {
                    dispatch({ type: ActionType.SET_PROVISIONING, isProvisioning: false});
                    dispatch({ type: ActionType.SET_PROVISION_STEP, step: ProvisionStep.BOOTSTRAPPING_CASSANDRA });
                }, 5000);
            }, 5000);
        }, 5000);
    };

    const onBtnClick = () => {
        provision(state.clusterInfo);
    };

    return (
        <div className={'actions-wrapper'}>
            <div className={'grey-btn'} onClick={onBtnClick}>SAVE</div>
        </div>
    );
};
