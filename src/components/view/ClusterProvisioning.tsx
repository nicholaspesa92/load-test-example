import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import Icon from '@material-ui/core/Icon';
import './ClusterProvisioning.scss';
import { ProvisionStep } from './../../models';

export const ClusterProvisioning = () => {
    return (
        <div className={'provisioning-wrapper'}>
            <div className={'title'}>
                <span>Provisioning Cluster</span>
                <div style={{flex: '1 1 100%'}} />
                <CircularProgress size={36} />
            </div>
            <div className={'step'}>
                <Icon>add</Icon>
                <span>{ProvisionStep.BOOTSTRAPPING_CASSANDRA}</span>
            </div>
            <div className={'step'}>
                <Icon>add</Icon>
                <span>{ProvisionStep.TOKEN_RANGES}</span>
            </div>
            <div className={'step'}>
                <Icon>add</Icon>
                <span>{ProvisionStep.COMMUNICATING_TO_CLUSTER}</span>
            </div>
        </div>
    );
};
