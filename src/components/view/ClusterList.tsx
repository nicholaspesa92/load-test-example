import React from 'react';
import { Link } from 'react-router-dom';
import './ClusterList.scss';
import { useAppState } from './../../store/AppContext';
import { Cluster } from '../../models';
import { ClusterCard } from './ClusterCard';

export const ClusterList = () => {

    const state = useAppState();

    return (
        <div className={'list-wrapper'}>
            {
                state.clusters.length === 0 &&
                <div className={'zero-header'}>
                    <span>No Clusters Created. Being creating your first cluster below</span>
                    <Link className={'grey-btn'} to={'/create-cluster'}>
                        <span>CREATE CLUSTER</span>
                    </Link>
                </div>
            }
            {
                state.clusters.map((cluster: Cluster, i: number) => {
                    return (
                        <ClusterCard key={i} index={i + 1} cluster={cluster} />
                    );
                })
            }
        </div>
    );
};
