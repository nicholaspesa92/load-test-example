import React, { useState, useEffect } from 'react';
import './ClusterLoadTest.scss';
import { Chart } from 'react-google-charts';

export const ClusterLoadTest = () => {

    let count = 0;
    const [nodes, setNodes] = useState(4);

    const initialData = [
        ['x', 'Reads', 'Writes'],
        [count, 0, 0],
    ];
    const [data, setData] = useState<(string[] | number[])[]>(initialData);
    const [isTesting, setTestingState] = useState(false);
    const [timer, setTimer] = useState<NodeJS.Timeout | null>(null);
    const [reads, setReads] = useState(3000);
    const [writes, setWrites] = useState(7000);

    useEffect(() => {
        if (isTesting) {
            setTimer(setInterval(() => {
                addData();
            }, 1000));
        }
        if (!isTesting) {
            if (timer) clearInterval(timer);
            setData(initialData);
        }
    }, [isTesting]);

    const onReadsChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        e.persist();
        setReads(e.target.valueAsNumber);
    };

    const onWritesChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        e.persist();
        setWrites(e.target.valueAsNumber);
    };

    const less = (e: React.MouseEvent<HTMLSpanElement>) => {
        setNodes(nodes === 0 ? 0 : nodes - 1);
    };

    const more = (e: React.MouseEvent<HTMLSpanElement>) => {
        setNodes(nodes + 1);
    };

    const randomInt = () => {
        return Math.floor(Math.random() * Math.floor(10000));
    }

    const addData = () => {
        count += 1;
        const clone = data;
        clone.push([count, randomInt(), randomInt()]);
        const added = [...clone];
        setData(added);
    };

    const test = () => {
        setTestingState(true);
    }

    const stop = () => {
        setTestingState(false);
    }

    return (
        <div className={'load-test-wrapper'}>
            <div className={'test-options'}>
                <div className={'test-option'}>
                    <span className={'header'}>{reads} Reads per second</span>
                    <input onChange={onReadsChange} type={'range'} min={2000} max={10000} defaultValue={3000} />
                </div>
                <div className={'test-option'}>
                    <span className={'header'}>{writes} Writes per second</span>
                    <input onChange={onWritesChange} type={'range'} min={2000} max={10000} defaultValue={7000} />
                </div>
                <div className={'test-option'}>
                    <span className={'header'}>Nodes</span>
                    <div className={'node-counter'}>
                        <span className={'node-action left'} onClick={less}>-</span>
                        <span className={'node-display'}>{nodes}</span>
                        <span className={'node-action right'} onClick={more}>+</span>
                    </div>
                </div>
                <button onClick={isTesting ? stop : test}>
                    {isTesting ? 'TERMINATE' : 'START TEST'}
                </button>
            </div>
            <div className={'test-display'}>
                <Chart
                    width={'100%'}
                    height={'150px'}
                    chartType={'LineChart'}
                    loader={<div>Loading Chart</div>}
                    data={data}
                    options={{
                        hAxis: {
                          title: 'Reads/Writes',
                        },
                        vAxis: {
                          title: 'Time',
                        },
                        series: {
                          1: { curveType: 'function' },
                        },
                    }}
                />
            </div>
        </div>
    );
};
