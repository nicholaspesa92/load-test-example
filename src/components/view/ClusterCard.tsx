import React, { useState } from 'react';
import './ClusterCard.scss';
import { Cluster } from '../../models';
import { ClusterLoadTest } from './ClusterLoadTest';
import { ClusterProvisioning } from './ClusterProvisioning';
import { useAppState } from './../../store/AppContext';

interface IClusterCard {
    index: number;
    cluster: Cluster;
}

export const ClusterCard = ({ index, cluster }: IClusterCard) => {

    const state = useAppState();

    const [isTesting, setTestState] = useState(true);

    const onLoadTest = (e: React.MouseEvent<HTMLButtonElement>) => {
        setTestState(!isTesting);
    }

    return (
        <div className={`cluster-card ${isTesting ? 'wide' : 'slim'}`}>
            <div className={'cluster-info'}>
                <span className={'card-header'}>Cluster #{index}</span>
                <span className={'section-header'}>Tier</span>
                <span className={'section-detail'}>{cluster.tier}</span>
                <span className={'section-header'}>Dataset</span>
                <span className={'section-detail'}>{cluster.dataset}</span>
                <div className={'cluster-actions'}>
                    {!state.provisioning.isProvisioning && <button onClick={onLoadTest}>{isTesting ? 'CLOSE TEST' : 'OPEN TEST'}</button>}
                </div>
            </div>
            {
                (isTesting && !state.provisioning.isProvisioning) &&
                <ClusterLoadTest />
            }
            {
                state.provisioning.isProvisioning &&
                <ClusterProvisioning />
            }
        </div>
    );
};
