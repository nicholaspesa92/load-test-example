import { ActionType, ClusterTier, Cluster, ClusterDataset, ProvisionStep } from './../models';

export type Action = { type: ActionType.SET_CLUTER_TIER; tier: ClusterTier }
    | { type: ActionType.ADD_CLUSTER; cluster: Cluster }
    | { type: ActionType.SET_CLUSTER_DATASET; dataset: ClusterDataset; }
    | { type: ActionType.SET_PROVISIONING; isProvisioning: boolean; }
    | { type: ActionType.SET_PROVISION_STEP; step: ProvisionStep; }

export type Dispatch = (action: Action) => void;
