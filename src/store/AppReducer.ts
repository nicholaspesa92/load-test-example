import { AppState } from './AppState';
import { Action } from './AppTypes';
import { ActionType } from './../models';

export const appReducer = (state: AppState, action: Action): AppState => {
    switch (action.type) {
        case ActionType.SET_CLUTER_TIER: {
            return {
                ...state,
                clusterInfo: {
                    ...state.clusterInfo,
                    tier: action.tier,
                },
            };
        }
        case ActionType.SET_CLUSTER_DATASET: {
            return {
                ...state,
                clusterInfo: {
                    ...state.clusterInfo,
                    dataset: action.dataset,
                },
            };
        }
        case ActionType.ADD_CLUSTER: {
            const clusters = [...state.clusters, action.cluster];
            return {
                ...state,
                clusters,
            }
        }
        case ActionType.SET_PROVISIONING: {
            return {
                ...state,
                provisioning: {
                    ...state.provisioning,
                    isProvisioning: action.isProvisioning,
                },
            }
        }
        case ActionType.SET_PROVISION_STEP: {
            return {
                ...state,
                provisioning: {
                    ...state.provisioning,
                    step: action.step,
                },
            }
        }
        default: {
            throw new Error(`Undefined action type`);
        }
    }
};
