import * as React from 'react';
import { AppState, initialAppState } from './AppState';
import { Dispatch } from './AppTypes';
import { appReducer } from './AppReducer';

export interface AppProviderProps {
    children: React.ReactNode;
}

const AppStateContext = React.createContext<AppState | undefined>(undefined);
const AppDispatchContext = React.createContext<Dispatch | undefined>(undefined);

const AppProvider = ({ children }: AppProviderProps) => {
    const [state, dispatch] = React.useReducer(appReducer, initialAppState);

    return (
        <AppStateContext.Provider value={state}>
            <AppDispatchContext.Provider value={dispatch}>{children}</AppDispatchContext.Provider>
        </AppStateContext.Provider>
    );
};

const useAppState = () => {
    const context = React.useContext(AppStateContext);
    if (!context) {
        throw new Error('useAppState must be used within an AppProvider');
    }
    return context;
};

const useAppDispatch = () => {
    const context = React.useContext(AppDispatchContext);
    if (!context) {
        throw new Error('useAppDispatch must be used within an AppProvider');
    }
    return context;
};

export { AppProvider, useAppState, useAppDispatch };
