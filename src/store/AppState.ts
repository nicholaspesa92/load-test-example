import { Cluster, ClusterTier, ClusterDataset, ProvisionStep } from './../models';

export interface AppState {
    clusterInfo: Cluster;
    clusters: Cluster[];
    provisioning: {
        isProvisioning: boolean;
        step: ProvisionStep;
    }
}

export const initialAppState: AppState = {
    clusterInfo: {
        tier: ClusterTier.STARTUP,
        dataset: ClusterDataset.US_CENSUS,
    },
    clusters: [],
    provisioning: {
        isProvisioning: false,
        step: ProvisionStep.BOOTSTRAPPING_CASSANDRA,
    },
};
