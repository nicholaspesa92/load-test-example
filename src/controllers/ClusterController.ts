import { useAppDispatch } from './../store/AppContext';
import { Cluster, ActionType, ProvisionStep } from '../models';
import history from './../history';

const provision = (cluster: Cluster) => {
    const dispatch = useAppDispatch();
    dispatch({ type: ActionType.ADD_CLUSTER, cluster });
    dispatch({ type: ActionType.SET_PROVISIONING, isProvisioning: true });
    setTimeout(() => {
        dispatch({ type: ActionType.SET_PROVISION_STEP, step: ProvisionStep.TOKEN_RANGES });
        setTimeout(() => {
            dispatch({ type: ActionType.SET_PROVISION_STEP, step: ProvisionStep.COMMUNICATING_TO_CLUSTER });
            setTimeout(() => {
                dispatch({ type: ActionType.SET_PROVISIONING, isProvisioning: false});
                dispatch({ type: ActionType.SET_PROVISION_STEP, step: ProvisionStep.BOOTSTRAPPING_CASSANDRA });
                history.push('/view-clusters');
            }, 5000);
        }, 5000);
    }, 5000);
};

export const ClusterCtrl = {
    provision,
};
