import React from 'react';
import { Router } from 'react-router-dom';
import './App.scss';
import { AppRoutes } from './routes';
import { AppProvider } from './store/AppContext';
import history from './history';

const App: React.FC = () => {
  return (
    <div className="App">
      <AppProvider>
        <Router history={history}>
          <AppRoutes />
        </Router>
      </AppProvider>
    </div>
  );
}

export default App;
