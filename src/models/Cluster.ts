export enum ClusterTier {
    STARTUP = 'startup',
    STANDARD = 'standard',
    ENTERPRISE = 'enterprise',
};

export enum ClusterDataset {
    US_CENSUS = 'US Census Data',
    SYNTHENTIC_USER = 'Syncthentic User Data'
};

export enum ProvisionStep {
    BOOTSTRAPPING_CASSANDRA = 'Bootstrapping Cassandra',
    TOKEN_RANGES = 'Determining Token Ranges',
    COMMUNICATING_TO_CLUSTER = 'Communicating to Cluster',
};

export interface Cluster {
    tier: ClusterTier;
    dataset: ClusterDataset;
}
 