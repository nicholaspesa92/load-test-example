export enum ActionType {
    SET_CLUTER_TIER = 'setClusterTier',
    ADD_CLUSTER = 'addCluster',
    SET_CLUSTER_DATASET = 'setClusterDataset',
    SET_PROVISIONING = 'setProvisioning',
    SET_PROVISION_STEP = 'setProvisionStep',
}
